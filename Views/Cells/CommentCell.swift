//
//  CommentCell.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 14.06.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var message: UILabel!
    
    @IBOutlet weak var imagevw: AvatarImage!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        selectionStyle = .none
    }
    
    var comment :Comment?
        {
        didSet{
            guard comment != nil
            else
            {
                return
            }
            
            imagevw.setting(isonline: true, radius: 25)
            if let link = comment?.sender?.pictUrl
            {
                imagevw.imageView.downloadedFrom(link: link)
                
            }
            imagevw.imageView.contentMode = .scaleAspectFit
            
            
            let name = comment?.sender?.name
            let secondName = comment?.sender?.secondName
            
            if (name != nil)&&(secondName != nil)
            {
                NameLabel.text = (comment?.sender?.name)! + " " + (comment?.sender?.secondName)!

            }
            
            if let text = comment?.body
            {
                message.text = text
            }
            
            
            
        }
        
    }

    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
