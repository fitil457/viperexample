//
//  postCell.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 12.04.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//




import Foundation
import TRMosaicLayout
import UIKit
protocol PostCellDelegate
{
    func showMenu(forPost post:WallPost)
    func showSender(forPost post:WallPost)
    
}
class PostCell:UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource
{
    @IBOutlet weak var avat: AvatarImage!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
    @IBOutlet weak var bodyContentView: UIView!
    
    
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var authorGestureRecognizer:UITapGestureRecognizer!
    
    var delegate:PostCellDelegate?
    
    var collectionSizes=[CGSize]()
    
    override func awakeFromNib() {

        separator.layer.borderColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1).cgColor
        selectionStyle = .none
        
        let nib = UINib(nibName: "PostPhotosCell", bundle: nil)
        
        collectionView.register(nib, forCellWithReuseIdentifier: "photoCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        
        authorGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapAuthor(_:)))
        //authorGestureRecognizer.cancelsTouchesInView = true
        avat.addGestureRecognizer(authorGestureRecognizer)
        nameLabel.addGestureRecognizer(authorGestureRecognizer)
       // timeLabel.addGestureRecognizer(authorGestureRecognizer)

    }
    func didTapAuthor(_ sender: Any) {
        guard post != nil else {
            return
        }
        delegate?.showSender(forPost: post!)
    }
    internal var post:WallPost?
    {
        didSet
        {
          //  DispatchQueue.global(qos: .userInteractive).async {
                if let post = self.post
                {
                    if let url = post.sender?.pictUrl
                    {
                        self.avat.imageView.downloadedFrom(link: url)
                        self.avat.setting(isonline: (post.sender?.isOnline)!, radius: 20)
                    }
                    if ( post.sender?.name != nil && post.sender?.secondName != nil)
                    {
                        self.nameLabel.text = (post.sender?.name)! + " " + (post.sender?.secondName)!
                    }
                    if let postTime = post.time
                    {
                        DispatchQueue.global(qos: .userInteractive).async{
                            let now = Date()
                            let dif = now.timeIntervalSince(postTime)
                            var text = ""
                            var formatter = DateFormatter()
                            switch dif
                            {
                            case 86400...172800 :
                                formatter.dateFormat = "dd MM YYYY HH:mm"
                                text = formatter.string(from: postTime)
                                
                            case 3600...86400:
                                formatter.dateFormat = "вчера HH:mm"
                                text = formatter.string(from: postTime)
                                
                            case 60...3600:
                                formatter.dateFormat = "сегодня HH:mm"
                                text = formatter.string(from: postTime)
                                
                            case 1...60:
                                text = Int(dif/60).description + "минут назад"
                            case 0...1:
                                text = "только что"
                            default:
                                formatter.dateFormat = "dd MM YYYY HH:mm"
                                text = formatter.string(from: postTime)
                            }
                            DispatchQueue.main.async {
                                self.timeLabel.text = formatter.string(from: postTime)
                            }
                        }
                    }
                    self.bodyLabel.text = post.id?.description
                    //post.body
                    
                    if post.pictureInfos != nil
                    {
                        self.collectionView.reloadData()
                    }
                    else
                    {
                        if post.map != nil
                        {
                            
                        }
                    }
            }
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! PostPhotosCell
        
        if let info = post?.pictureInfos?[indexPath.row]
        {
            cell.photoInfo = info
            
        }
        return cell
    }
    
    
    
   /* func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! PostPhotosCell
        
        if let info = post.pictureInfos?[indexPath.row]
        {
            cell.photoInfo = info

        }
        //cell.backgroundColor = UIColor.black
      //  cell.setcell(friend: profile.friends[indexPath.row])
        return cell
    }*/
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard post?.pictureInfos != nil else {
            return 0
        }
        return (post?.pictureInfos?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2
       // let size = post.pictureInfos?[indexPath.row].size
        
        UIScreen.main.bounds.width
        let picDimension = UIScreen.main.bounds.width / 4.0
        return CGSize(width: picDimension, height: picDimension)
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
 
    // 4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    
   
    
    @IBAction func menuShow(_ sender: Any) {
        
        guard post != nil else {
            return
        }
        delegate?.showMenu(forPost:post!)
        
    }
    
    
}


