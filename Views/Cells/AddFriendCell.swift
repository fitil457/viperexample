//
//  friendCell.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 07.08.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import UIKit

import Foundation
import  UIKit
class AddFriendCell:UITableViewCell
{
    
    @IBOutlet weak var avat: AvatarImage!
    @IBOutlet weak var name: UILabel!
    
    var friend:Person?
    {
        didSet
        {
            if let friend = self.friend
            {
                avat.imageView.contentMode =  .scaleAspectFit
                
                UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
                UIFont.systemFont(ofSize: 15, weight: UIFontWeightBold)
                
                var nm = NSAttributedString(string: friend.name!, attributes: [NSFontAttributeName:UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)])
                var sn = NSAttributedString(string: " " + friend.secondName!, attributes: [NSFontAttributeName:UIFont.systemFont(ofSize: 15, weight: UIFontWeightBold)])
                var st = NSMutableAttributedString(attributedString: nm)
                st.append(sn)
                self.name.attributedText = st
                //self.name.textColor = UIColor.blue
                
                avat.setting(isonline: true, radius: 25)
                if let url = friend.pictUrl
                {
                    avat.imageView.downloadedFrom(link: url)
                    
                }
                else
                {
                    avat.imageView = UIImageView()
                    
                }
            }
            
        }
    }
    
    
    
    
    
    @IBAction func AddFriend(_ sender: Any) {
        if let id = friend?.id
        {
            ApiManager.shared.addFriend(id: friend!.id!, completion: { (ApiRequestStatus) in
                
            })
        }
        
    }
}
