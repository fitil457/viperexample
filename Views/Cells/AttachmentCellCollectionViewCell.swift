//
//  AttachmentCellCollectionViewCell.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 03.05.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import UIKit
protocol AttacmentCellDelegate
{
    func deleteAttachment(attachment:Attachment)
}
class AttachmentCellCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    var delegate:AttacmentCellDelegate?
    var attachment:Attachment?
    {
        didSet
        {
            attachment?.set(imageView: imageView)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func deleteAttachment(_ sender: Any) {
        if attachment != nil
        {
            delegate?.deleteAttachment(attachment: attachment!)
        }
    }
}
