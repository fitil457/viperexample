//
//  profileVC.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 05.04.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import Foundation
import UIKit




class profileVC:UITableViewController
{
    var postListPresenter: PostListPresenterProtocol!
    var profilePresenter: ProfilePresenterProtocol?
    @IBOutlet weak var settingsBut: UIBarButtonItem!
    var profileProperty:FullProfile?
    var isLoadingProfile = false
    var isLoadingPosts = false
    var isProfileLoaded = false
    var isPostsLoaded = false

    override func viewDidLoad(){
        super.viewDidLoad()
        setTableView()
        profilePresenter?.viewDidLoad()
        postListPresenter.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        postListPresenter.viewWillAppear()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "main") as! profileMainCell
            cell.profile = profileProperty
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "addPostCell") as! AddPostCell
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "wallPostCell") as! PostCell
            if let post = postListPresenter?.postBy(index: indexPath.row)
            {
                cell.delegate = self
                cell.post = post
            }
            return cell
        default: return UITableViewCell()
        }
    }
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 2
        {
            postListPresenter.viewWillDisplayObject(number: indexPath.row)
        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       switch section {
        case 0:
            if profileProperty != nil
            {
                return 1
            }
            else
            {
                return 0
            }
        case 1:
            return 1
        case 2:
            return postListPresenter.postsCount
        default:
            return 0
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1
        {
            postListPresenter?.showAddPostInterface()
        }
        if indexPath.section == 2 , let post = (tableView.cellForRow(at: indexPath) as? PostCell)?.post
        {
            postListPresenter?.showDetailForPost(post: post)
        }
    }
    
    @IBAction func toSettings(_ sender: Any) {
        performSegue(withIdentifier: "profileToSettings", sender: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func setTableView()
    {
        tableView.register(UINib(nibName: "AddPostCell", bundle: Bundle.main), forCellReuseIdentifier: "addPostCell")
        tableView.register(UINib(nibName: "ProfileMainCell", bundle: Bundle.main), forCellReuseIdentifier: "main")
        tableView.register(UINib(nibName: "PostCell", bundle: Bundle.main), forCellReuseIdentifier: "wallPostCell")
        tableView.estimatedRowHeight = 440
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.sectionFooterHeight =  0
        tableView.sectionHeaderHeight =  0
        tableView.tableHeaderView = nil
        tableView.tableFooterView?.frame = CGRect.zero
        tableView.estimatedSectionHeaderHeight = 0
    }
}

extension profileVC:ProfileViewProtocol
{

    func showProfile(with profile: FullProfile)
    {
        tableView.beginUpdates()
            isProfileLoaded = true
            profileProperty = profile
            tableView.reloadSections(IndexSet(integersIn: 0...0), with: UITableViewRowAnimation.automatic)
        tableView.endUpdates()
        
    }
    
    func showProfileError(){}
    
    func showProfileLoading(){
        isLoadingProfile = true
    }
    
    func hideProfileLoading(){
        isLoadingProfile = false
    }
}
extension profileVC:PostListViewProtocol
{
    func schouldShowError(status:ApiRequestStatus,activity:PostActivities)
    {
        var alertTitle = ""
        var alertText = ""
        switch status {
        case .noConnection:
            alertText = "Нет соединения"
        case .noRights:
            alertText = "Нет прав"
        default:
            break
        }
        switch activity {
        case .postAdding:
            alertTitle = "Ошибка создания записи"
        case .postListRefreshing:
            alertTitle = "Ошибка обновления списка записей"
        case .postEditing:
            alertTitle = "Ошибка редактирования записи"
        case .postDeleting:
            alertTitle = "Ошибка удаления записи"

        default:
            break
        }
        let alertController = UIAlertController(title: alertTitle, message: alertText, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.destructive) { (UIAlertAction) in
            alertController.dismiss(animated: true, completion: {
                
            })
        }
        alertController.addAction(okAction)
        present(alertController, animated: true) { 
            
        }
    }
    func showLoading()
    {
        DispatchQueue.main.async {
            
        }
    }

    func hideLoading()
    {
        DispatchQueue.main.async {
            
        }
    }

    
    func showMoreEntitiesAtTheEnd(Amount:Int)
    {
        DispatchQueue.main.async {
            let ip = self.tableView.indexPathsForVisibleRows![0]
            let beforeRect = self.tableView.rectForRow(at:ip)
 
            let set = IndexPath.setOfIndexPaths(startRow: UInt(self.tableView.numberOfRows(inSection: 2)) , count: UInt(Amount), in: 2)
           // self.tableView.insertRows(at: set, with: UITableViewRowAnimation.automatic)
            self.tableView.reloadData()
            //self.tableView.endUpdates()
            let afterRect = self.tableView.rectForRow(at:ip)

            
           // self.tableView.contentOffset.y += afterRect.minY - beforeRect.minY
            
        }
    }
    func showEntities()
    {
        DispatchQueue.main.async {
            //self.tableView.beginUpdates()
            print(self.tableView.numberOfSections)
            
            self.tableView.reloadSections(IndexSet(integer: 2), with: UITableViewRowAnimation.automatic)
            //self.tableView.endUpdates()
        }
    }

    func showMoreEntitiesAtTheStart(Amount:Int)
    {
        DispatchQueue.main.async {
            self.tableView.beginUpdates()
            let set = IndexPath.setOfIndexPaths(startRow: 0, count: UInt(Amount), in: 2)
            self.tableView.insertRows(at: set, with: UITableViewRowAnimation.automatic)
            self.tableView.endUpdates()
        }
        
    }

    func shouldReloadPostViews(atIndexes:[Int])
    {
        DispatchQueue.main.async {
            //self.tableView.beginUpdates()
            var indexPaths = [IndexPath]()
            for index in atIndexes
            {
                indexPaths.append(IndexPath(row: index, section: 2))
            }
            self.tableView.reloadRows(at: indexPaths, with: .automatic)
        }
    }

    func shouldDeletePostViews(atIndexes:[Int])
    {
        DispatchQueue.main.async
        {
            self.tableView.beginUpdates()
            var indexPaths = [IndexPath]()
            for index in atIndexes
            {
                indexPaths.append(IndexPath(row: index, section: 2))
            }
            self.tableView.deleteRows(at: indexPaths, with: .automatic)
            self.tableView.endUpdates()
        }
    }
}
extension profileVC:PostCellDelegate
{
    func showMenu(forPost post:WallPost)
    {
        let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        let deleteAction = UIAlertAction(title: "Удалить", style: UIAlertActionStyle.destructive) { (UIAlertAction) in
            self.postListPresenter?.deletePost(post: post)
        }
        let editAction = UIAlertAction(title: "Редактировать", style: UIAlertActionStyle.default) { (UIAlertAction) in
            self.postListPresenter?.showEditPostInterface(post: post)
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: UIAlertActionStyle.cancel) { (UIAlertAction) in
            alert.dismiss(animated: true, completion: {
                
            })
        }
        
        alert.addAction(deleteAction)
        alert.addAction(editAction)
        alert.addAction(cancelAction)
        present(alert, animated: true) {
            
        }
    }
    func showSender(forPost post:WallPost)
    {
        postListPresenter?.showSenderProfileForPost(post: post)
    }
    
  
    
}
extension profileVC:profileMainCellDelegate
{
    func goToInformation() {
        performSegue(withIdentifier: "toInformation", sender: nil)
    }
    func showOnMap(){}
    func changeFriendStatus(){}
    func writeMessage(){}
}
