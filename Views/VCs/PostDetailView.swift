//
// Created by VIPER
// Copyright (c) 2017 VIPER. All rights reserved.
//

import Foundation
import UIKit

class PostDetailView: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    var postDetailPresenter: PostDetailPresenterProtocol?
    var postCommentsPresenter: PostCommentsPresenterProtocol?
    var commentsLoader:CommentsLoaderByPostId!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var commentField: UITextField!
    @IBOutlet weak var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    
    var post:WallPost!
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func viewDidLoad() {
        
        postCommentsPresenter?.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        setTableView()
    }
    
    func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let beginFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            let dif = -(endFrame?.minY)!+(beginFrame?.minY)!
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant += dif
            } else {
                self.keyboardHeightLayoutConstraint?.constant += dif            }
            UIView.animate(withDuration: duration,
                           delay: 0,
                           options: animationCurve,
                           animations:
                {
                    self.view.layoutIfNeeded()
                    if self.tableView.contentSize.height - self.tableView.contentOffset.y - self.tableView.frame.height < -dif
                    {
                        self.tableView.contentOffset.y-=self.tableView.contentSize.height - self.tableView.contentOffset.y - self.tableView.frame.height
                    }
                    else
                    {
                        self.tableView.contentOffset.y += dif
                    }
            },
                           completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
        if (indexPath.section == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "wallPostCell") as! PostCell
            return cell
        }
        else
        {
            if (indexPath.row ==  postCommentsPresenter!.commentsCount()-10) {
                print("schould load more")
                postCommentsPresenter?.viewDidScrollToLastLoadedComments()
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "Comment") as! CommentCell
            if let comment = postCommentsPresenter?.commentBy(index: indexPath.row)
            {
                cell.comment = comment
            }
            return cell
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return 1
        }
        else
        {
            if let cnt = postCommentsPresenter?.commentsCount()
            {
                return cnt
            }
            else
            {
                return 0
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
    }
    func didLoadEntitiesToTheEnd(Amount: Int) {
        
    }
    func didLoadEntitiesToTheStart(Amount: Int) {
        
    }
    func didLoadEntities(Amount: UInt) {
        tableView.reloadSections(IndexSet(integersIn: 1...1), with: UITableViewRowAnimation.automatic)
    }
    func didReloadEntities(indexes:[Int])
    {}
    func didDeleteEntities(indexes:[Int])
    {}
    func didAddEntities(indexes:[Int])
    {}
    @IBAction func addComent(_ sender: Any) {
        postCommentsPresenter?.addComment(text: commentField.text!)
    }
    func setTableView()
    {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "PostCell", bundle: Bundle.main), forCellReuseIdentifier: "wallPostCell")
        
        tableView.register(UINib(nibName: "CommentCell", bundle: Bundle.main), forCellReuseIdentifier: "Comment")
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.separatorStyle = .none
    }
}
extension PostDetailView:PostDetailViewProtocol
{
    func showPost(post:WallPost){}
    func removeFromViewHierarchy()
    {
        if let nav = navigationController
        {
            nav.popViewController(animated: true)
        }
        else
        {
            dismiss(animated: true, completion: {
                
            })
        }
    }

}

extension PostDetailView:PostCommentsViewProtocol
{
    func showLoading(){}
    func hideLoading(){}
    
    func showMoreCommentsAtTheEnd(Amount:Int)
    {
        DispatchQueue.main.async {
            self.tableView.beginUpdates()
            let set = IndexPath.setOfIndexPaths(startRow: UInt(self.tableView.numberOfRows(inSection: 1)) , count: UInt(Amount), in: 1)
            self.tableView.insertRows(at: set, with: UITableViewRowAnimation.automatic)
            self.tableView.endUpdates()
            
        }
    }
    func showComments()
    {
        DispatchQueue.main.async {
            //self.tableView.beginUpdates()
            print(self.tableView.numberOfSections)
            
            self.tableView.reloadSections(IndexSet(integer: 1), with: UITableViewRowAnimation.automatic)
            //self.tableView.endUpdates()
            
        }
    }
    func showMoreCommentsAtTheStart(Amount:Int)
    {
        DispatchQueue.main.async {
            self.tableView.beginUpdates()
            let set = IndexPath.setOfIndexPaths(startRow: 0, count: UInt(Amount), in: 1)
            self.tableView.insertRows(at: set, with: UITableViewRowAnimation.automatic)
            self.tableView.endUpdates()
            
        }
    }
    func shouldReloadCommentsViews(atIndexes:[Int]){}
    func shouldDeleteCommentsViews(atIndexes:[Int]){}
}
