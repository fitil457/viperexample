//
// Created by VIPER
// Copyright (c) 2017 VIPER. All rights reserved.
//

import Foundation
import UIKit


import UIKit
import BSImagePicker
import Photos

protocol AttachmentsDelegate {
    func deleteAttachment(for indexPath: IndexPath)
}
class PostAddView: UIViewController,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,AttacmentCellDelegate {
    
    var presenter: PostAddPresenterProtocol?

    @IBOutlet weak var fromBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var attachmentsCollection: UICollectionView!
    @IBOutlet weak var attachmentsHeightConstraint: NSLayoutConstraint!
    
    let barButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: "readyPushed")
    let attachmentUpdatesQueue = DispatchQueue(label: "lab", qos: .utility)
    var profile:FullProfile?
    var post:WallPost?
    @IBOutlet weak var separator: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        presenter?.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    func setView()
    {
        textView.delegate = self
        attachmentsCollection.delegate = self
        attachmentsCollection.dataSource = self
        attachmentsCollection.register(UINib(nibName: "AttachmentCellCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "attachmentCell")
        navigationItem.setRightBarButton(barButtonItem, animated: true)

    }
    func readyPushed()
    {
        presenter?.readyTaped(text: textView.text)
    }
    override func viewDidAppear(_ animated: Bool) {
        textView.becomeFirstResponder()
    }
    func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let beginFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
          
            self.fromBottomConstraint?.constant =  view.frame.height - (endFrame?.minY)! + 100
            UIView.animate(withDuration: duration,
                           delay: 0,
                           options: animationCurve,
                           animations:
                {
                    self.view.layoutIfNeeded()
                },
                           completion: nil)
            
            
        }
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        
    }
    
    
    @IBAction func addAttachmentsPushed(_ sender: Any) {
        let alertView = UIAlertController(title: "attach", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        let addPhotoAction = UIAlertAction(title: "Фото", style: UIAlertActionStyle.default) { (UIAlertAction) in
            
        let photosSelectVC = BSImagePickerViewController()
            self.navigationController?.bs_presentImagePickerController(photosSelectVC, animated: true,
                                                                                            select: { (asset: PHAsset) -> Void in
                                                                                                // User selected an asset.
                                                                                                // Do something with it, start upload perhaps?
            }, deselect: { (asset: PHAsset) -> Void in
                // User deselected an assets.
                // Do something, cancel upload?
            }, cancel: { (assets: [PHAsset]) -> Void in
                // User cancelled. And this where the assets currently selected.
            }, finish: { (assets: [PHAsset]) -> Void in
                self.presenter?.addPicturesAttachments(assets: assets)

                // User finished with these assets
            }, completion: nil)
            
        }
        let addCoordinatesAction = UIAlertAction(title: "местоположение", style: UIAlertActionStyle.default) { (UIAlertAction) in
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (UIAlertAction) in
            
        }
        alertView.addAction(addPhotoAction)
        alertView.addAction(addCoordinatesAction)
        alertView.addAction(cancelAction)
        present(alertView, animated: true, completion: nil)
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        guard presenter != nil else {
            return 0
        }
        
        return presenter!.attachmentsCount()
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = attachmentsCollection.dequeueReusableCell(withReuseIdentifier: "attachmentCell",for: indexPath) as! AttachmentCellCollectionViewCell
        
        //cell.backgroundColor = UIColor.black
        let att = presenter?.attachmentBy(index:indexPath.row )
        
        cell.delegate = self
        cell.attachment = att
        // Configure the cell
        return cell
    }
    /*func deleteAttachment(for indexPath: IndexPath) {
     DispatchQueue.global().async {
     self.attachments.remove(at: indexPath.row)
     
     self.attachmentsCollection.deleteItems(at: [indexPath])
     }
     
     
     }*/
    func deleteAttachment(attachment:Attachment)
    {
        self.presenter?.deleteAttachment(attachment: attachment)

        self.attachmentsHeightConstraint.constant = attachmentsCollection.collectionViewLayout.collectionViewContentSize.height

    }
    
}
extension PostAddView:PostAddViewProtocol
{
    func showPost(post:WallPost)
    {
        textView.text = post.body
        
    }
    func deletedAttachment(index:Int){}
    func showMoreAttachments(starting:Int,amount: Int)
    {
        self.attachmentsCollection.insertItems(at: IndexPath.setOfIndexPaths(startRow: UInt(starting), count: UInt(amount), in: 0))
        self.attachmentsHeightConstraint.constant = attachmentsCollection.collectionViewLayout.collectionViewContentSize.height
    }
    func showLoadingOnAttachment(index:Int){}
    func hideLoadingOnAttachment(index:Int){}
    func removeFromViewHierarchy()
    {
        if let nav = navigationController
        {
            nav.popViewController(animated: true)
        }
        else
        {
            dismiss(animated: true, completion: {
            })
        }
    }
}
