//
//  PostAddAssembly.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 21.07.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import UIKit

class PostAddAssembly:PostAddAssemblyProtocol
{
    class func generateEditModule(post:WallPost,_ output:PostAddModuleOutput?) -> UIViewController?
    {
        var presenter: PostAddPresenterProtocol&PostAddInteractorOutputProtocol = PostEditPresenter(post: post)
        return generate(presenter: presenter, output: output)
    }
    class func generateAddModule(wallId:Int,_ output:PostAddModuleOutput?) -> UIViewController?
    {
        var presenter: PostAddPresenterProtocol&PostAddInteractorOutputProtocol = PostAddPresenter(wallId: wallId)
        return generate(presenter: presenter, output: output)        
    }
    class func generate(presenter:PostAddPresenterProtocol&PostAddInteractorOutputProtocol,output:PostAddModuleOutput?) -> UIViewController?
    {
        guard let mainStoryboard = getMainStoryBoard()
            else {
                return nil
        }
        if let view = mainStoryboard.instantiateViewController(withIdentifier:"AddPostVC") as? PostAddViewProtocol
        {
            let interactor: PostAddInteractorInputProtocol = PostAddInteractor()
            
            var wireFrame: PostAddWireFrameProtocol = PostAddWireFrame()
            
            // Connecting
            view.presenter = presenter
            interactor.presenter = presenter
            wireFrame.presenter = presenter
            
            presenter.view = view
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            presenter.output = output
            
            interactor.APIDataManager = ApiManager.shared
        
            return view as? UIViewController
        }
        else
        {
            return nil
        }
        

    }
}
