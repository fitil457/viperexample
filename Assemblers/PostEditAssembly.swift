//
//  PostEditAssembly.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 09.08.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import UIKit
class PostEditAssembly:PostEditAssemblyProtocol
{
    class func generateEditModule(post:WallPost,_ output:PostAddModuleOutput?) -> UIViewController?
    {
        // Generating module components
        guard let mainStoryboard = getMainStoryBoard()
            else {
                return nil
        }
        if let view = mainStoryboard.instantiateViewController(withIdentifier:"AddPostVC") as? PostAddViewProtocol
        {
            let presenter: PostAddPresenterProtocol&PostAddInteractorOutputProtocol = PostEditPresenter(post: post)
            let interactor: PostAddInteractorInputProtocol = PostAddInteractor()
            let wireFrame: PostAddWireFrameProtocol = PostAddWireFrame()
            // Connecting
            view.presenter = presenter
            interactor.presenter = presenter
            wireFrame.presenter = presenter
            
            presenter.view = view
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            presenter.output = output
            
            interactor.APIDataManager = ApiManager.shared
            
            return view as? UIViewController
        }
        else
        {
            return nil
        }
    }
}
