//
//  AssemblyProtocols.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 09.08.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import UIKit
protocol PostDetailAssemblyProtocol: class
{
    static func generatePostDetailModule(post:WallPost,_ output: PostDetailOutput?) -> UIViewController?
}
protocol ProfileAssemblyProtocol: class
{
    static func createProfileModule(withId:Int)->UIViewController?
}
protocol PostAddAssemblyProtocol: class
{
    static func generateAddModule(wallId:Int,_ output:PostAddModuleOutput?) -> UIViewController?
}
protocol PostEditAssemblyProtocol: class
{
    static func generateEditModule(post:WallPost,_ output:PostAddModuleOutput?) -> UIViewController?
}
