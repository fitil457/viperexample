//
//  PostDetailAssembly.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 11.07.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import UIKit
class PostDetailAssembly: PostDetailAssemblyProtocol
{
    class func generatePostDetailModule(post:WallPost,_ output:PostDetailOutput?) -> UIViewController?
    {
        // Generating module components
        guard let mainStoryboard = getMainStoryBoard()
            else {
                return nil
        }
        guard post.id != nil
            else
        {
            return nil
        }
        if let view = mainStoryboard.instantiateViewController(withIdentifier:"PostDetailVC") as? PostDetailViewProtocol&PostCommentsViewProtocol
        {
            var postDetailPresenter: protocol<PostDetailPresenterProtocol, PostDetailInteractorOutputProtocol> = PostDetailPresenter(post: post)
            var postCommentsPresenter: PostCommentsPresenterProtocol & PostCommentsInteractorOutputProtocol = PostCommentsPresenter()
            var postDetailInteractor: PostDetailInteractorInputProtocol = PostDetailInteractor()
            
            var postCommentsInteractor: PostCommentsInteractorInputProtocol = PostCommentsInteractor(postId: post.id!)
            
            var wireFrame: PostDetailWireFrameProtocol = PostDetailWireFrame()
            
            // Connecting
            view.postDetailPresenter = postDetailPresenter
            view.postCommentsPresenter = postCommentsPresenter

            
            postDetailPresenter.view = view
            postDetailPresenter.wireFrame = wireFrame
            postDetailPresenter.interactor = postDetailInteractor
            
            postDetailInteractor.presenter = postDetailPresenter
            
            postCommentsPresenter.view = view
            postCommentsPresenter.interactor = postCommentsInteractor
            
            postCommentsInteractor.presenter = postCommentsPresenter
            
            postCommentsInteractor.APIDataManager = ApiManager.shared
            postDetailInteractor.APIDataManager = ApiManager.shared

            return view as? UIViewController
        }
        else
        {
            return nil
        }
        
        
        
    }
}
