//
//  ProfileAssembly.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 07.07.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import UIKit
class ProfileAssembly:ProfileAssemblyProtocol
{
    class func createProfileModule(withId: Int) -> UIViewController? {
        guard let mainStoryboard = getMainStoryBoard()
        else {
                return nil
        }
        if let view = mainStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as? ProfileViewProtocol & PostListViewProtocol
            {
                let profilePresenter: ProfilePresenterProtocol & ProfileInteractorOutputProtocol = ProfilePresenter()
                let profileInteractor: ProfileInteractorInputProtocol  = ProfileInteractor(id: withId)
                let profileWireFrame: ProfileRouterProtocol = ProfileRouter()
                
                let postListWireFrame: PostListRouterProtocol = PostListRouter()
                let postListPresenter: PostListPresenterProtocol & PostListInteractorOutputProtocol = PostListPresenter()
                let postListInteractor: PostListInteractorInputProtocol  = PostListInteractor(wallOwnerId: withId)

                view.postListPresenter = postListPresenter
                view.profilePresenter = profilePresenter
                
                profilePresenter.view = view
                postListPresenter.view = view

                postListPresenter.router = postListWireFrame
                profilePresenter.router = profileWireFrame

                profilePresenter.interactor = profileInteractor
                profileInteractor.presenter = profilePresenter
                
                postListPresenter.interactor = postListInteractor
                postListInteractor.presenter = postListPresenter
                
                
                profileInteractor.id = withId
                postListInteractor.wallOwnerId = withId
                
                profileWireFrame.presenter = profilePresenter
                postListWireFrame.presenter = postListPresenter
                
                profileInteractor.dataManager = ApiManager.shared
                postListInteractor.apiManager = ApiManager.shared
                return view as? UIViewController
            }
        return nil
    }
}
