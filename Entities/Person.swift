//
//  Person.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 28.04.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import UIKit

class Person:DictionaryEntity
{
    
    lazy var id:Int? = self.dictionary["id"] as? Int
    lazy var name:String? = self.dictionary["first_name"] as? String
    lazy var secondName:String? = self.dictionary["last_name"] as? String
    lazy var pictUrl:String? = self.dictionary["avatar"] as? String
    lazy var lastLoginTime:Date? = (self.dictionary["last_login"] as? String)?.dateFromISO8601
    var isOnline:Bool
    {
        get
        {
            guard lastLoginTime != nil else {
                return false
            }
            return Date().timeIntervalSince(lastLoginTime!) > 3000
        }
    }
}
