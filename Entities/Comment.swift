//
//  Comment.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 14.06.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import Foundation
class Comment:DictionaryEntity
{
    lazy var id:Int? = self.dictionary["id"] as? Int
    lazy var sender:Person? = Person(with: (self.dictionary["owner"] as? [String:Any]))
    lazy var body:String? = self.dictionary["text"] as? String
    lazy var time:Date? = (self.dictionary["datetime"] as? String)?.dateFromISO8601    
    override init(with dictionary:[String:Any])
    {
        super.init(with: dictionary)
    }
    init(text:String)
    {
        var dict = ["text":text,"attachments":NSMutableArray()] as [String:Any]
        super.init(with: dict)
    }
    
}
