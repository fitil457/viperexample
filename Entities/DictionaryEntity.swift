//
//  DictionaryEntity.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 18.08.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import UIKit
class DictionaryEntity
{
    var dictionary:[String:Any]
    init?(with dictionary: [String:Any]?)
    {
        guard let dict = dictionary else
        {
            return nil
        }
        self.dictionary = dictionary!
    }
    init(with dictionary: [String:Any])
    {
        self.dictionary = dictionary
    }
}
