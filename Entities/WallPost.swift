//
//  WallPost.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 29.04.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import UIKit



class WallPost:DictionaryEntity
{
    lazy var id:Int? = self.dictionary["id"] as? Int
    lazy var sender:Person? = Person(with: (self.dictionary["owner"] as? [String:Any]))
    lazy var body:String? = self.dictionary["text"] as? String
    lazy var time:Date? = (self.dictionary["datetime"] as? String)?.dateFromISO8601
    var map:MapDirection?
    var pictureInfos:[PictureFromNetInfo]?
    init (text:String = "",attachments:NSOrderedSet? = nil)
    {
        var dict = ["text":text] as [String : Any]
        var filesArray = [Any]()
        if attachments != nil
        {
            filesArray.reserveCapacity(attachments!.count)
            for attachment in attachments!
            {
                if let fileAttachment = attachment as? AttachmentWithFile
                {
                    filesArray.append(fileAttachment.fileURL)
                }
                else
                {
                    filesArray.append((attachment as? Attachment)?.body)
                }
            }
        }
        dict["files"] = filesArray
        super.init(with: dict)
    }
    override init?(with dictionary: [String : Any]?) {
        super.init(with: dictionary)
    }
    override init(with dictionary: [String : Any]) {
        super.init(with: dictionary)
    }
}





