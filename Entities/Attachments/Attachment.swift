//
//  Attachment.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 04.05.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import UIKit
class Attachment
{
    var body:[String:Any]
    var title:String?
    func set(imageView:UIImageView)
    {
        DispatchQueue.main.async {
            imageView.image = UIImage(named: "ФонРегистрации.png")
        }
    }
    init(dict:[String:Any])
    {
        body = dict
    }
}
class AttachmentWithFile:Attachment
{
    var fileURL:String?
    var fileIsLoaded = false
}
