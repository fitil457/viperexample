//
//  CommentsApiManager.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 07.08.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import Alamofire
extension ApiManager:PostCommentsDataManagerProtocol
{
    func addComment(postId:Int,comment:Comment,completion:@escaping(_ comment:[String:Any]?,_ status:ApiRequestStatus)->Void)
    {
        
        let postData:[String:Any]? = comment.dictionary as? [String : Any]
        
        var headers = [String:String]()
        addCSRFHeaderTo(headers: &headers)
        Alamofire.request("http://188.225.38.189/blogs/posts/\(postId)", method: .put, parameters: postData, encoding: JSONEncoding.default, headers:headers).responseJSON
        {(response) in
                
            let dataAndStatus = self.generateDataAndStatusAndErrors(jsonResponse: response, type: [String:Any].self)
            completion(dataAndStatus.value,dataAndStatus.status)
        }
        
        
    }
    func editComment(commentId:Int,completion:@escaping(_ newPost:[String:Any]?,_ status:ApiRequestStatus)->Void){}
    func deleteComment(commentId:Int,completion:@escaping(_ status:ApiRequestStatus)->Void)
    {
        
    }
    
    func loadComments(postId:Int,completion:@escaping(_ comments:[[String:Any]]?,_ status:ApiRequestStatus)->Void)
    {
        let postfix = "/blogs/posts/" + postId.description
        Alamofire.SessionManager.default
            .requestWithoutCache("\(ApiManager.domain)\(postfix)").responseJSON { response in
                let dataAndStatus = self.generateDataAndStatusAndErrors(jsonResponse: response, type: [[String:Any]].self)
                completion(dataAndStatus.value,dataAndStatus.1)
        }
    }
    
    func loadMoreOldComments(postId:Int,beforeCommentId:Int,completion completionHandler:@escaping(_ posts:[[String:Any]]?,_ status:ApiRequestStatus)->Void){}
    func loadMoreNewComments(postId:Int,afterCommentId:Int,completion completionHandler:@escaping(_ posts:[[String:Any]]?,_ status:ApiRequestStatus)->Void){}
}
