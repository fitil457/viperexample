//
//  AuthorizationApiManager.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 26.07.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import Alamofire

extension ApiManager:AuthorizationProviderProtocol
{
    func register(dict:[String:Any],comletion: @escaping (_ errors:[String:Any]?,_ status:ApiRequestStatus)->())
    {
        let registerData = dict
        Alamofire.request("\(ApiManager.domain)registration.php/?sec=rnVa-4B2xdXfH2vJ", method: .post, parameters: registerData as! Parameters).responseJSON
        { (response) in
            
            self.saveCookies(response: response)

            let statusAndErrors = self.generateStatusAndErrors(response: response)
            comletion(statusAndErrors.errors,statusAndErrors.status)
        }
    }
    func login(name:String,password:String,comletion: @escaping (_ profile:[String:Any]?,_ errors:[String:Any]?,_ status:ApiRequestStatus)->())
    {
        let registerData = ["email":name,"password":password]
        Alamofire.request("\(ApiManager.domain)/persons/sign_in", method: .post, parameters: registerData).responseJSON
        { (response) in
            
            self.saveCookies(response: response)
            
            let dataAndStatusAndErrors = self.generateDataAndStatusAndErrors(jsonResponse: response, type: [String:Any].self)
            comletion(dataAndStatusAndErrors.value,dataAndStatusAndErrors.errors,dataAndStatusAndErrors.status)
        }
    }
    func checkLogin(comletion: @escaping(_ profile:[String:Any]?,_ errors:[String:Any]?,_ status:ApiRequestStatus)->())
    {
        
        var headers = [String:String]()
        addCSRFHeaderTo(headers: &headers)
        Alamofire.request(ApiManager.domain,headers: headers).responseJSON
        { (response) in
            let dataAndStatusAndErrors = self.generateDataAndStatusAndErrors(jsonResponse: response, type: [String:Any].self)
            comletion(dataAndStatusAndErrors.value,dataAndStatusAndErrors.errors,dataAndStatusAndErrors.status)
        }
        
    }
    func logout()
    {
        UserDefaults.standard.set(nil, forKey: "savedCookies")
        UserDefaults.standard.synchronize()
    }
}
