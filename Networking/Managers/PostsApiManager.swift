//
//  WallApiManager.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 26.07.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import Alamofire
extension ApiManager:WallPostsDataManagerProtocol
{
    
    func loadPosts(wallId:Int,comletion:@escaping (_ posts:[[String:Any]]?,_ status:ApiRequestStatus)->Void)
    {
        URLCache.shared.removeAllCachedResponses()
        let postfix = "/blogs/" + wallId.description
    
        Alamofire.SessionManager.default.requestWithoutCache("\(ApiManager.domain)\(postfix)").responseJSON { response in
            
            let dataAndStatus = self.generateDataAndStatusAndErrors(jsonResponse: response, type: [[String:Any]].self,schouldPrint: true)
            comletion(dataAndStatus.value,dataAndStatus.status)
        }
    }
    func loadMoreOldPosts(wallId:Int,beforePostId:Int,comletion:@escaping(_ posts:[[String:Any]]?,_ status:ApiRequestStatus)->Void)
    {
        let postfix = "/blogs/\(wallId.description)?position=\(beforePostId.description)&count=10"
        Alamofire.SessionManager.default
            .requestWithoutCache("\(ApiManager.domain)\(postfix)").responseJSON { response in
        
            let dataAndStatus = self.generateDataAndStatusAndErrors(jsonResponse: response, type: [[String:Any]].self)
            comletion(dataAndStatus.value,dataAndStatus.1)
        }
    }
    func loadMoreNewPosts(wallId:Int,afterPostId:Int,comletion:@escaping(_ posts:[[String:Any]]?,_ status:ApiRequestStatus)->Void)
    {
        let postfix = "/blogs/\(wallId.description)?position=\(afterPostId.description)&count=10"
        Alamofire.SessionManager.default
            .requestWithoutCache("\(ApiManager.domain)\(postfix)").responseJSON { response in
                
                let dataAndStatus = self.generateDataAndStatusAndErrors(jsonResponse: response, type: [[String:Any]].self)
                comletion(dataAndStatus.value,dataAndStatus.1)
        }
    }
    func deletePost(postId:Int,completion:@escaping (_ status:ApiRequestStatus)->Void)
    {
        var headers = [String:String]()
        addCSRFHeaderTo(headers: &headers)
        Alamofire.request("http://188.225.38.189/blogs/posts/\(postId)", method: .delete, headers: headers).responseJSON
        {
                
            response in
                
            let statusAndErrors = self.generateStatusAndErrors(response: response)
            completion(statusAndErrors.status)
            
        }
    }
    func editPost(postId:Int,newPost:WallPost,comletion:@escaping (_ newPost:[String:Any]?,_ status:ApiRequestStatus)->Void)
    {
        var headers = [String:String]()
        addCSRFHeaderTo(headers: &headers)
        
        Alamofire.request("\(ApiManager.domain)/blogs/posts/\(postId)", method: .patch, parameters: newPost.dictionary as! Parameters, encoding: JSONEncoding.default, headers:headers).responseJSON
        {
            response in
                
            let dataAndStatusAndErrors = self.generateDataAndStatusAndErrors(jsonResponse: response, type: [String:Any].self)
            comletion(dataAndStatusAndErrors.value,dataAndStatusAndErrors.status)
        }

    }
    func addPost(wallId:Int,post:WallPost,completion:@escaping(_ newPost:[String:Any]?,_ status:ApiRequestStatus)->Void)
    {
        var postData:NSMutableDictionary = NSMutableDictionary.init(dictionary: post.dictionary)
        var headers = [String:String]()
        addCSRFHeaderTo(headers: &headers)
        Alamofire.request("http://188.225.38.189/blogs/\(wallId)", method: .put, parameters: postData as! [String:Any], encoding: JSONEncoding.default, headers:headers).responseJSON
        {
            response in                
            let dataAndStatusAndErrors = self.generateDataAndStatusAndErrors(jsonResponse: response, type: [String:Any].self)
            completion(dataAndStatusAndErrors.value,dataAndStatusAndErrors.status)
        }
    }
}
