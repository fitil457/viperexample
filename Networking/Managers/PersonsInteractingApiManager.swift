//
//  PersonsInteractingApiManager.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 09.08.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import Alamofire

extension ApiManager:PersonsInteractingManagerProtocol
{
    func addFriend(id:Int,completion:@escaping (_ status:ApiRequestStatus)->Void)
    {
        let data = ["id":id] as [String : Any]
        var headers = [String:String]()
        addCSRFHeaderTo(headers: &headers)
    
        Alamofire.request("http://188.225.38.189/persons/friends", method: .put, parameters: data, encoding: JSONEncoding.default,headers:headers).responseJSON
        { (response) in
            let statusAndErrors = self.generateStatusAndErrors(response: response)
            completion(statusAndErrors.status)
        }
    }

}
