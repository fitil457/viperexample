//
//  ProfileApiManager.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 26.07.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import Alamofire
extension ApiManager:ProfileDataManagerProtocol
{
    
    func loadProfile(id:Int, comletion: @escaping ((_ profile:[String:Any]?,_ status:ApiRequestStatus)->()))
    {
        Alamofire.request("\(ApiManager.domain)/persons/\(id.description)").responseJSON { response in
            let dataAndStatus = self.generateDataAndStatusAndErrors(jsonResponse: response, type: [String:Any].self)
            comletion(dataAndStatus.value,dataAndStatus.status)
        }
    }
}
