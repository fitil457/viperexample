//
//  ApiManager.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 17.07.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//
enum ApiRequestStatus
{
    case noRights
    case noConnection
    case success
    case unknownError
    case errors
    case unauthorized
    case wrongData
    case notFound
}


import Alamofire
import Foundation
class ApiManager
{
    
    static let shared = ApiManager()
    static let domain = "http://188.225.38.189"
    
    func addCSRFHeaderTo( headers:inout [String:String])
    {
        let cookieJar = HTTPCookieStorage.shared
        
        if let myCookies = cookieJar.cookies(for: URL(string: ApiManager.domain)!)
        {
            if myCookies.count != 0
            {
                headers["X-CSRFToken"] = myCookies[0].value
            }
        }
        
    }
    func printRequestAndResponse(response: DataResponse<Any>)
    {
        print("___________________\n")
        print("SEND:")
        print(response.request?.url)
        print(response.request?.httpMethod)
        print(response.request?.allHTTPHeaderFields)
        if let dta = response.request?.httpBody
        {
            print (NSString(data: dta, encoding: String.Encoding.utf8.rawValue))
        }
        print("___________________")
        print("RECV:")
        if (response.response != nil)
        {
            print (response.response)
            
        }
        print(response.result.value.debugDescription)
        
    }
    
    func generateDataAndStatusAndErrors<T>(jsonResponse:DataResponse<Any>,type:T.Type,schouldPrint:Bool = true) -> (value:T?,status:ApiRequestStatus,errors:[String:Any]?)
    {
        if schouldPrint
        {
            printRequestAndResponse(response: jsonResponse)
        }
        
        if 200...299 ~= jsonResponse.response!.statusCode && jsonResponse.result.isSuccess
        {
            if let json = jsonResponse.result.value as? T
            {
                print("SUCCESS")
                return (json,.success,nil)
                
            }
            else
            {
                print("WRONG DATA")
                return (nil,.wrongData,nil)
            }
        }
        else
        {
            let json = jsonResponse.result.value as? [String:Any]
            
            if let err = jsonResponse.result.error as? URLError, err.code  == URLError.Code.notConnectedToInternet
            {
                print("NO CONNECTION")
                return (nil,.noConnection,nil)
            }
            
            guard let code = jsonResponse.response?.statusCode
                else
            {
                return (nil,.unknownError,nil)
            }
            
            switch code {
            case 404:
                print("NOT FOUND")
                return (nil,.notFound,json)

            case 403:
                print("NO RIGHTS")
                return (nil,.noRights,json)
            case 401:
                print("UNAUTHORIZED")
                return (nil,.unauthorized,json)
            default:
                let status = (json != nil) ? ApiRequestStatus.errors : ApiRequestStatus.unknownError
                print(status)
                return (nil,status,json)
            }

            
        }
        
    }
    func generateStatusAndErrors(response:DataResponse<Any>,schouldPrint:Bool = true) -> (status:ApiRequestStatus,errors:[String:Any]?)
    {
        if schouldPrint
        {
            printRequestAndResponse(response: response)
        }
        if 200...299 ~= response.response!.statusCode
        {
            print("SUCCESS")
            return (.success,nil)
                
        }
        else
        {
            if let err = response.result.error as? URLError, err.code  == URLError.Code.notConnectedToInternet
            {
                print("NO CONNECTION")
                return (.noConnection,nil)
            }
            
            let json = response.result.value as? [String:Any]
            
            
            
            guard let code = response.response?.statusCode
                else
            {
                print("UNKNOWN ERROR")
                return (.unknownError,json)
            }
            
            switch code {
            case 404:
                print("NOT FOUND")
                return (.notFound,json)
            case 403:
                print("NO RIGTHS")
                return (.noRights,json)
            case 401:
                print("UNAUTHORIZED")
                return (.unauthorized,json)
            default:
                let status = (json != nil) ? ApiRequestStatus.errors : ApiRequestStatus.unknownError
                print(status)
                return (status,json)
                
            }
            
        }
        
    }
    func loadCookies() {
        guard let cookieArray = UserDefaults.standard.array(forKey: "savedCookies") as? [[HTTPCookiePropertyKey: Any]] else { return }
        for cookieProperties in cookieArray {
            if let cookie = HTTPCookie(properties: cookieProperties) {
                HTTPCookieStorage.shared.setCookie(cookie)
            }
        }
    }
    func saveCookies(response: DataResponse<Any>) {
        let headerFields = response.response?.allHeaderFields as! [String: String]
        let url = response.response?.url
        let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerFields, for: url!)
        var cookieArray = [[HTTPCookiePropertyKey: Any]]()
        for cookie in cookies {
            cookieArray.append(cookie.properties!)
        }
        UserDefaults.standard.set(cookieArray, forKey: "savedCookies")
        UserDefaults.standard.synchronize()
    }
    
}




