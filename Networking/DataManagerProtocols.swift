//
//  Protocols.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 09.08.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import Foundation

protocol PersonsInteractingManagerProtocol {
    func addFriend(id:Int,completion:@escaping (_ status:ApiRequestStatus)->Void)
}
protocol AuthorizationProviderProtocol {
    func login(name:String,password:String,comletion: @escaping (_ profile:[String:Any]?,_ errors:[String:Any]?,_ status:ApiRequestStatus)->())
    func logout()
    func checkLogin(comletion: @escaping(_ profile:[String:Any]?,_ errors:[String:Any]?,_ status:ApiRequestStatus)->())
    func register(dict:[String:Any],comletion: @escaping (_ errors:[String:Any]?,_ status:ApiRequestStatus)->())
}
protocol ProfileDataManagerProtocol{
 
    func loadProfile(id:Int, comletion: @escaping ((_ profile:[String:Any]?,_ status:ApiRequestStatus)->()))
}
protocol WallPostsDataManagerProtocol{
    func loadPosts(wallId:Int,comletion:@escaping(_ posts:[[String:Any]]?,_ status:ApiRequestStatus)->Void)
    func loadMoreOldPosts(wallId:Int,beforePostId:Int,comletion:@escaping(_ posts:[[String:Any]]?,_ status:ApiRequestStatus)->Void)
    func loadMoreNewPosts(wallId:Int,afterPostId:Int,comletion:@escaping(_ posts:[[String:Any]]?,_ status:ApiRequestStatus)->Void)
    func deletePost(postId:Int,completion:@escaping(_ status:ApiRequestStatus)->Void)
    func editPost(postId:Int,newPost:WallPost,comletion:@escaping(_ newPost:[String:Any]?,_ status:ApiRequestStatus)->Void)
    func addPost(wallId:Int,post:WallPost,completion:@escaping(_ newPost:[String:Any]?,_ status:ApiRequestStatus)->Void)
}
protocol PostCommentsDataManagerProtocol {
    func addComment(postId:Int,comment:Comment,completion:@escaping(_ comment:[String:Any]?,_ status:ApiRequestStatus)->Void)
    func editComment(commentId:Int,completion:@escaping(_ newPost:[String:Any]?,_ status:ApiRequestStatus)->Void)
    func deleteComment(commentId:Int,completion:@escaping(_ status:ApiRequestStatus)->Void)
    
    func loadComments(postId:Int,completion:@escaping(_ posts:[[String:Any]]?,_ status:ApiRequestStatus)->Void)
    
    func loadMoreOldComments(postId:Int,beforeCommentId:Int,completion:@escaping(_ posts:[[String:Any]]?,_ status:ApiRequestStatus)->Void)
    func loadMoreNewComments(postId:Int,afterCommentId:Int,completion:@escaping(_ posts:[[String:Any]]?,_ status:ApiRequestStatus)->Void)
}
