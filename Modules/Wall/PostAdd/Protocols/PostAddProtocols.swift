//
// Created by VIPER
// Copyright (c) 2017 VIPER. All rights reserved.
//

import Foundation
import Photos

protocol PostAddViewProtocol: class
{
    var presenter: PostAddPresenterProtocol? { get set }
    func showPost(post:WallPost)
    func deletedAttachment(index:Int)
    func showMoreAttachments(starting:Int,amount:Int)
    func showLoadingOnAttachment(index:Int)
    func hideLoadingOnAttachment(index:Int)
    func removeFromViewHierarchy()
}
protocol PostAddWireFrameProtocol: class
{
    var presenter:PostAddPresenterProtocol?{ get set }
}
protocol PostAddPresenterProtocol: class
{
    var view: PostAddViewProtocol? { get set }
    var interactor: PostAddInteractorInputProtocol? { get set }
    var wireFrame: PostAddWireFrameProtocol? { get set }
    var output:PostAddModuleOutput? { get set }
    
    func readyTaped(text:String)
    func addPicturesAttachments(assets: [PHAsset])
    func viewDidLoad()
    func addAttachment()
    func deleteAttachment(attachment:Attachment)
    func attachmentBy(index:Int)->Attachment?
    func attachmentsCount() ->Int
}
protocol PostAddInteractorOutputProtocol: class
{
    func didLoadPost(post:[String:Any], wallId:Int)
    func didEditPost(post:[String:Any])
    func didLoadAttachmentToServer(attachment:Attachment)
}
protocol PostAddInteractorInputProtocol: class
{
    var presenter: PostAddInteractorOutputProtocol? { get set }
    var APIDataManager: WallPostsDataManagerProtocol? { get set }
    
    func loadPictureAttachmentToServer(attachment:PhotoAttachmentFromDevice)
    func loadFileAttachmentToServer(attachment:Attachment)
    func editPost(oldPost:WallPost,newPost:WallPost)
    func addPost(post:WallPost, wallId:Int)

}
protocol PostAddModuleOutput: class
{
    func didAddPost(post:[String:Any])
    func editedPost(oldPost:WallPost,newPostDictionary:[String:Any])
}

