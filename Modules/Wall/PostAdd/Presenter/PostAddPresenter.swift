//
// Created by VIPER
// Copyright (c) 2017 VIPER. All rights reserved.
//

import Foundation
import Photos
class PostAddPresenterBase:PostAddPresenterProtocol,PostAddInteractorOutputProtocol
{
    weak var view: PostAddViewProtocol?
    var interactor: PostAddInteractorInputProtocol?
    var wireFrame: PostAddWireFrameProtocol?
    var output:PostAddModuleOutput?
    var attachmentsSet = NSMutableOrderedSet()
    var attachmentsInLoadingCount = 0
    func viewDidLoad()
    {
        
    }
    func addPicturesAttachments(assets: [PHAsset])
    {
        DispatchQueue.global().async {
            let startingCount = self.attachmentsSet.count
            for asset in assets
            {
                let attachment = PhotoAttachmentFromDevice(with: asset)
                self.attachmentsSet.add(attachment)
                self.interactor?.loadPictureAttachmentToServer(attachment: attachment)
                self.attachmentsInLoadingCount += 1
            }
            
            DispatchQueue.main.async {
                
                self.view?.showMoreAttachments(starting:startingCount,amount: assets.count)
                for i in (startingCount-1)..<self.attachmentsSet.count
                {
                    self.view?.showLoadingOnAttachment(index: i)
                }
            }
        }
    }

    
    
    
    func readyTaped(text:String)
    {
        
    }
    
    func addAttachment()
    {
        
    }
    func deleteAttachment(attachment:Attachment){}
    func attachmentBy(index:Int)->Attachment?
    {
        guard (index >= 0)&&(index < attachmentsSet.count) else {
            return nil
        }
        return attachmentsSet[index] as? Attachment
    }
    func attachmentsCount() ->Int
    {
        return attachmentsSet.count
    }
    func didLoadAttachmentToServer(attachment:Attachment)
    {
        self.attachmentsInLoadingCount -= 1
    }

    
    
    func didLoadPost(post:[String:Any], wallId:Int)
    {
        output?.didAddPost(post: post)
        view?.removeFromViewHierarchy()
    }
    func didEditPost(post:[String:Any])
    {
        view?.showPost(post: WallPost(with: post))
    }
    
    
}

class PostAddPresenter: PostAddPresenterBase
{
    
    var wallId:Int = 0
    
    init(wallId:Int)
    {
        self.wallId = wallId
    }
    override func readyTaped(text:String)
    {
        if attachmentsInLoadingCount == 0
        {
            let post = WallPost(text: text, attachments: attachmentsSet)
            interactor?.addPost(post: post, wallId: wallId)
        }
    }
    
    
    
    
}
class PostEditPresenter: PostAddPresenterBase
{
    var post:WallPost
    init(post:WallPost)
    {
        self.post = post
    }
    override func viewDidLoad() {
        view?.showPost(post: post)
    }
    override func readyTaped(text:String)
    {
        let newPost = WallPost(text: text,
                               attachments: attachmentsSet)
        interactor?.editPost(oldPost: self.post,
                             newPost: newPost)
    }
    override func didEditPost(post:[String:Any])
    {
        output?.editedPost(oldPost: self.post, newPostDictionary: post)
        view?.removeFromViewHierarchy()
    }
    
}
