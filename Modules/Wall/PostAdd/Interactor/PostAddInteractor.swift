//
// Created by VIPER
// Copyright (c) 2017 VIPER. All rights reserved.
//

import Foundation
import Photos
class PostAddInteractor: PostAddInteractorInputProtocol
{
    weak var presenter: PostAddInteractorOutputProtocol?
    var APIDataManager: WallPostsDataManagerProtocol? 
    func editPost(oldPost:WallPost,newPost:WallPost)
    {
        if let id = oldPost.id
        {
            APIDataManager?.editPost(postId: id,newPost: newPost, comletion: {[weak self] (postDictinary, status) in
                guard let strSelf = self
                else
                {
                    return
                }
                switch status
                {
                case .success:
                        if postDictinary != nil
                        {
                            strSelf.presenter?.didEditPost(post: postDictinary!)
                        }
                case .noConnection:
                    break
                default:
                    break
                }
            })
        }
    }
    func addPost(post:WallPost, wallId:Int)
    {
        APIDataManager?.addPost(wallId: wallId, post: post, completion: {[weak self] (value,status) in
            guard let strSelf = self
                else
            {
                return
            }
            if status == .success
            {
                strSelf.presenter?.didLoadPost(post: value!, wallId: wallId)
            }
        })
    }
    
    func loadPictureAttachmentToServer(attachment:PhotoAttachmentFromDevice)
    {
        let phManager = PHImageManager()
        phManager.requestImageData(for: attachment.phAsset, options: nil, resultHandler: { (data:Data?, str:String?, orientation:UIImageOrientation, arg:[AnyHashable : Any]?) -> Void in
            if data != nil{
                let jpeg = UIImageJPEGRepresentation(UIImage(data: data!)!, 1)
                ServerManager.shared(named: "main")?.POSTFormRequestByAdding(postfix: "/persons/files", data: jpeg!, complititionHandler:
                    { (data:Data?, response:URLResponse?, error:Error?) in
                    
                        if let httpResponse = response as? HTTPURLResponse, let fields = httpResponse.allHeaderFields as? [String : String]
                        {
                            if httpResponse.statusCode == 201
                            {
                                if let JSONData = data
                                {
                                    do
                                    {
                                        if let dict = try JSONSerialization.jsonObject(with: JSONData, options: .mutableContainers) as? NSDictionary
                                        {
                                            attachment.fileURL = (dict["file"] as! String)
                                        }
                                    }
                                    catch{}
                                }
                                
                                attachment.fileIsLoaded = true
                                self.presenter?.didLoadAttachmentToServer(attachment: attachment)
                            }
                            else
                            {
                                print("did not add file")
                            }
                        }
                },withCookies: true,with:"PUT")
            }
        })

    }
    func loadFileAttachmentToServer(attachment:Attachment){}
}
