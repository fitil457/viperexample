//
// Created by VIPER
// Copyright (c) 2017 VIPER. All rights reserved.
//

import Foundation

protocol PostCommentsViewProtocol: class
{
    var postCommentsPresenter: PostCommentsPresenterProtocol? { get set }
    func showLoading()
    func hideLoading()
    func showMoreCommentsAtTheEnd(Amount:Int)
    func showComments()
    func showMoreCommentsAtTheStart(Amount:Int)
    func shouldReloadCommentsViews(atIndexes:[Int])
    func shouldDeleteCommentsViews(atIndexes:[Int])
}

protocol PostCommentsWireFrameProtocol: class
{
    var presenter:PostAddPresenterProtocol?{get set}
    func goToSenderProfile(from view: PostCommentsViewProtocol, forComment comment: Comment)
}

protocol PostCommentsPresenterProtocol: class
{
    var view: PostCommentsViewProtocol? { get set }
    var interactor: PostCommentsInteractorInputProtocol? { get set }
    var wireFrame: PostCommentsWireFrameProtocol? { get set }
    
    func commentBy(index:Int)->Comment?
    func commentsCount()->Int
    
    func viewDidLoad()
    func viewWillAppear()
    func viewDidScrollToLastLoadedComments()
    func viewWantsToResfreshComments()
    
    func addComment(text:String)
    func editComment(comment: Comment)
    func deleteComment(comment: Comment)
    func showDetailForComment(comment:Comment)
    func showSenderProfileForComment(comment:Comment)
}

protocol PostCommentsInteractorOutputProtocol: class
{
    func loadEntities(data:[[String:Any]])
    func loadEntitiesToTheEnd(data:[[String:Any]])
    func loadEntitiesToTheStart(data:[[String:Any]])

    func deletedComment(post:Comment)
    func editedComment(oldPost:Comment,newPostDictionary:[String:Any])
}

protocol PostCommentsInteractorInputProtocol: class
{
    var presenter: PostCommentsInteractorOutputProtocol? { get set }
    var APIDataManager: PostCommentsDataManagerProtocol? { get set }
    var postId: Int { get set }
    func addComment(comment:Comment)
    func load()
    func loadMoreAfter(comment:Comment)
    func loadMoreBefore(comment:Comment)
    func edit(comment:Comment)
    func delete(comment:Comment)
}




    
