//
// Created by VIPER
// Copyright (c) 2017 VIPER. All rights reserved.
//

import Foundation

class PostCommentsPresenter: PostCommentsPresenterProtocol
{
    weak var view: PostCommentsViewProtocol?
    var interactor: PostCommentsInteractorInputProtocol?
    var wireFrame: PostCommentsWireFrameProtocol?
    fileprivate var postComments = NSMutableOrderedSet()
    func commentBy(index:Int)->Comment?
    {
        guard (index >= 0)&&(index < postComments.count) else {
        return nil
        }
        if let dictionary = postComments[index] as? [String:Any]
        {
            return Comment(with: dictionary)
        }
        return nil
    }
    func commentsCount()->Int{
        return postComments.count
    }
    func viewDidLoad()
    {
        interactor?.load()
    }
    func viewWillAppear(){}
    func viewDidScrollToLastLoadedComments(){}
    func viewWantsToResfreshComments(){}
    func addComment(text:String)
    {
        
        let comment = Comment(text: text)
        interactor?.addComment(comment: comment)
    }
    func editComment(comment: Comment){}
    func deleteComment(comment: Comment){}
    func showDetailForComment(comment:Comment){}
    func showSenderProfileForComment(comment:Comment){}
}
extension PostCommentsPresenter:PostCommentsInteractorOutputProtocol
{
    func loadEntities(data:[[String:Any]])
    {
        self.postComments = NSMutableOrderedSet(array: data)
        self.view?.hideLoading()
        self.view?.showComments()
    }
    
    func loadEntitiesToTheEnd(data:[[String:Any]])
    {
        let count = data.count
        self.postComments.addObjects(from: data)
        view?.showMoreCommentsAtTheEnd(Amount: count)
    }
    func loadEntitiesToTheStart(data:[[String:Any]])
    {
        let count = data.count
        let set = IndexSet(integer: 0)
        self.postComments.insert(data, at: set)
        view?.showMoreCommentsAtTheStart(Amount: count)
    }
    func deletedComment(post:Comment){}
    func editedComment(oldPost:Comment,newPostDictionary:[String:Any]){}
}
