//
// Created by VIPER
// Copyright (c) 2017 VIPER. All rights reserved.
//

import UIKit

class PostCommentsWireFrame: PostCommentsWireFrameProtocol
{
    weak var presenter:PostAddPresenterProtocol?

    func goToSenderProfile(from view: PostCommentsViewProtocol, forComment comment: Comment)
    {
        guard let id = (comment.sender?.id)
            else
        {
            return
        }
        if let vc = ProfileAssembly.createProfileModule(withId: id)
        {
            if let vc0  = view as? UIViewController
            {
                vc0.show(vc, sender: nil)
            }
        }
    }
}

