//
// Created by VIPER
// Copyright (c) 2017 VIPER. All rights reserved.
//

import Foundation

class PostCommentsInteractor: PostCommentsInteractorInputProtocol
{
    weak var presenter: PostCommentsInteractorOutputProtocol?
    var APIDataManager: PostCommentsDataManagerProtocol?
    var postId:Int
    func load()
    {
        let arr:[Any] = [1,2,3,45,6]
        let arr2 = Array.init(repeating: 2, count: arr.count)
        let currentId = postId
        APIDataManager?.loadComments(postId: postId, completion: {[weak self] (array:[[String:Any]]?, status:ApiRequestStatus) in
            guard let strSelf = self
                else
            {
                return
            }
            if status == .success
            {
                strSelf.presenter?.loadEntities(data: array!)
            }
        })  
    }
    func addComment(comment:Comment)
    {
        APIDataManager?.addComment(postId: postId, comment: comment, completion: {[weak self] (dictionary, status) in
            guard let strSelf = self
                else
            {
                return
            }
            if status == .success
            {
                strSelf.presenter?.loadEntitiesToTheEnd(data: [dictionary!])
            }
            else
            {
                
            }
        })
    }
    func loadMoreAfter(comment:Comment)
    {
        
    }
    func loadMoreBefore(comment:Comment)
    {
        
    }
    func edit(comment:Comment)
    {
        
    }
    func delete(comment:Comment)
    {
        
    }
    init(postId:Int)
    {
        self.postId = postId
    }
}
