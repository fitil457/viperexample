//
//  WallRouter.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 04.07.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import UIKit

class PostListRouter: PostListRouterProtocol {
    weak var presenter:PostListPresenterProtocol?
    static var mainStoryboard: UIStoryboard
    {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    func goToSenderProfile(from view: PostListViewProtocol, forPost post: WallPost){
        guard let id = (post.sender?.id)
        else
        {
            return
        }
        if let vc = ProfileAssembly.createProfileModule(withId: id)
        {
            if let vc0  = view as? UIViewController
            {
                vc0.show(vc, sender: nil)
            }
        }
    }
    func goToPostDetail(from view: PostListViewProtocol, forPost post: WallPost)
    {
        
        if let vc = PostDetailAssembly.generatePostDetailModule(post: post, presenter as? PostDetailOutput)
        {
            if let vc0  = view as? UIViewController
            {
                vc0.show(vc, sender: nil)
            }
        }
    }
    func goToAddPost(from view: PostListViewProtocol,wallId:Int){
        if let vc = PostAddAssembly.generateAddModule(wallId: wallId, presenter as? PostAddModuleOutput)
        {
            if let vc0  = view as? UIViewController
            {
                vc0.show(vc, sender: nil)
            }
        }
       
    }
    func goToEditPost(from view: PostListViewProtocol,forPost post:WallPost)
    {
        if let vc = PostAddAssembly.generateEditModule(post: post, presenter as? PostAddModuleOutput)
        {
            if let vc0  = view as? UIViewController
            {
                vc0.show(vc, sender: nil)
            }
        }
    }
    
    
}
