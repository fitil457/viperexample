//
//  WallPostServerLoader.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 01.05.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import UIKit

class PostListInteractor:PostListInteractorInputProtocol
{

    weak var presenter: PostListInteractorOutputProtocol?
    var apiManager:WallPostsDataManagerProtocol?
    var wallOwnerId:Int!
    var serverManager:ServerManager!
   
    private var reconnectingTimer: Timer? = nil
    {
        willSet
        {
            reconnectingTimer?.invalidate()
        }
    }
    init(wallOwnerId:Int)
    {
        self.wallOwnerId = wallOwnerId
    }
    func load()
    {
        let currentId = wallOwnerId != nil ? wallOwnerId : -1
        apiManager?.loadPosts(wallId: currentId!) {[weak self] (array:[[String:Any]]?, status:ApiRequestStatus) in
            guard let strSelf = self
                else
            {
                return
            }
            if status == .success
            {
                strSelf.presenter?.loadEntities(data: array! )
                strSelf.didReconnect()
            }
            else
            {
                strSelf.loadingError(status: status, activity: PostActivities.postListRefreshing)
            }
        }

    }
    func loadMoreAfter(post:WallPost)
    {
        guard let id = post.id
            else
        {
            return
        }
        apiManager?.loadMoreNewPosts(wallId: wallOwnerId, afterPostId: id)
        {[weak self] (array:[[String:Any]]?, status:ApiRequestStatus) in
            guard let strSelf = self
                else
            {
                return
            }
            if status == .success
            {
                strSelf.presenter?.loadEntitiesToTheStart(data: array! )
            }
            else
            {
                strSelf.loadingError(status: status, activity: PostActivities.postListRefreshing)
            }
        }
    }
    func loadMoreBefore(post:WallPost)
    {
        guard let id = post.id
        else
        {
            return
        }
        apiManager?.loadMoreOldPosts(wallId: wallOwnerId, beforePostId: id)
        {[weak self] (array:[[String:Any]]?, status:ApiRequestStatus) in
            guard let strSelf = self
                else
            {
                return
            }
            if status == .success
            {
                strSelf.presenter?.loadEntitiesToTheEnd(data: array!)
                
            }
            else
            {
                strSelf.loadingError(status: status, activity: PostActivities.postListRefreshing)
            }
        }
    
    }
    
    func edit(post:WallPost){}
    func delete(post:WallPost)
    {
        guard let id = post.id else {
            return
        }
        
        apiManager?.deletePost(postId: id, completion: {[weak self] (status) in
            guard let strSelf = self
            else
            {
                return
            }
            if status == .success
            {
                strSelf.presenter?.deletedPost(post: post)
            }
            else
            {
                strSelf.loadingError(status: status, activity: PostActivities.postDeleting)
            }
        })

        
    }
    func loadingError(status:ApiRequestStatus, activity: PostActivities)
    {
        self.presenter?.someError(status: status, activity: activity)
        if status == .noConnection
        {
            self.reconnectingTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.reconnect), userInfo: nil, repeats: true)
        }
        else
        {
            self.didReconnect()
        }
    }
    
    @objc func reconnect() {
        load()
    }
    func didReconnect()
    {
        reconnectingTimer = nil
    }
    deinit {
        reconnectingTimer = nil
    }
    
}




