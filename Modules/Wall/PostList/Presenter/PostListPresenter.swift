//
//  WallPostsPresenter.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 04.07.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//
import UIKit


class PostListPresenter: PostListPresenterProtocol {
    weak var view: PostListViewProtocol?
    var interactor: PostListInteractorInputProtocol?
    var router: PostListRouterProtocol?
    var isLoadingMoreNew = false
    var isLoadingMoreOld = false
    var didLoadTheOldestPost = false  
    fileprivate var wallPosts = NSMutableOrderedSet()

    // VIEW -> PRESENTER
    
    func postBy(index:Int)->WallPost?
    {
        guard (index >= 0)&&(index < wallPosts.count) else
        {
            return nil
        }
        return WallPost(with: wallPosts[index] as? [String:Any])
    }
    
    var postsCount:Int
    {
        get
        {
            return wallPosts.count
        }
    }
    
    func viewDidLoad()
    {
    }
    func viewWillAppear()
    {
        if wallPosts.count != 0
        {
            viewWantsToResfreshPosts()
        }
        else
        {
            self.interactor?.load()
        }
    }
    func viewWillDisplayObject(number:Int)
    {
        if (number == wallPosts.count * 2 / 3) || (number == wallPosts.count)
        {
            viewDidScrollToLastLoadedPosts()
        }
    }
    func viewDidScrollToLastLoadedPosts()
    {
        guard !isLoadingMoreOld else {
            return
        }
        guard !didLoadTheOldestPost else {
            return
        }
        if let post = postBy(index: wallPosts.count-1)
        {
            isLoadingMoreOld = true
            interactor?.loadMoreBefore(post: post)
        }
    }
    func viewWantsToResfreshPosts()
    {
        guard !isLoadingMoreNew else {
            return
        }
        if let post = postBy(index: wallPosts.count-1)
        {
            isLoadingMoreNew = true
            self.interactor?.loadMoreAfter(post: post)
        }
    }
    
    func showEditPostInterface(post: WallPost)
    {
        guard view != nil else {
            return
        }
        self.router?.goToEditPost(from: self.view!, forPost: post)
    }
    func deletePost(post: WallPost)
    {
        interactor?.delete(post: post)
    }
    func showDetailForPost(post: WallPost)
    {
        guard view != nil else {
            return
        }
        router?.goToPostDetail(from: view!, forPost: post)
    }
    func showSenderProfileForPost(post:WallPost)
    {
        guard view != nil
        else
        {
            return
        }
        router?.goToSenderProfile(from: view!, forPost: post)
    }
    func showAddPostInterface()
    {
        if let view = view , let interactor = interactor
        {
            router?.goToAddPost(from: view, wallId: interactor.wallOwnerId)
        }
    }
}
extension PostListPresenter: PostListInteractorOutputProtocol,PostDetailOutput,PostAddModuleOutput {
    
    
    func someError(status:ApiRequestStatus,activity:PostActivities)
    {
        if status == .noConnection
        {
            view?.showLoading()
        }
        
        if activity == .postListRefreshing && status == .noConnection
        {
            isLoadingMoreNew = false
            isLoadingMoreOld = false
        }
        else
        {
            view?.schouldShowError(status:status,activity:activity)
        }
    }
    
    func loadEntities(data:[[String:Any]])
    {

        self.wallPosts = NSMutableOrderedSet(array: data)
        self.view?.hideLoading()
        self.view?.showEntities()
        
    }
    func loadEntitiesToTheEnd(data:[[String:Any]])
    {
        self.isLoadingMoreOld = false

        guard data.count != 0 else {
            didLoadTheOldestPost = true
            return
        }
        let count0 = self.wallPosts.count
        self.wallPosts.addObjects(from: data)
        self.view?.showMoreEntitiesAtTheEnd(Amount: self.wallPosts.count - count0)
        
        
    }
    func loadEntitiesToTheStart(data:[[String:Any]])
    {
        self.isLoadingMoreNew = false

        guard data.count != 0 else {
            return
        }
        let postsCount0 = self.wallPosts.count
        let count = data.count
        let set = IndexSet(integersIn: 0..<count)
        self.wallPosts.insert(data, at: set)
        self.view?.showMoreEntitiesAtTheStart(Amount: self.wallPosts.count - postsCount0)
    }
    func deletedPost(post:WallPost)
    {
        let index = self.wallPosts.index(of: post.dictionary)
        self.wallPosts.removeObject(at: index)
        self.view?.shouldDeletePostViews(atIndexes: [index])
    }
    func editedPost(oldPost:WallPost,newPostDictionary:[String:Any])
    {
        let index = self.wallPosts.index(of: oldPost.dictionary)
        self.wallPosts[index] = newPostDictionary
        self.view?.shouldReloadPostViews(atIndexes: [index])
    }
    func didAddPost(post:[String:Any])
    {
        self.wallPosts.insert(post, at: 0)
        self.view?.showMoreEntitiesAtTheStart(Amount: 1)
    }
        
}

