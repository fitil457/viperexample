//
//  WallProtocols.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 04.07.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

import UIKit
enum PostActivities
{
    case postAdding
    case postListRefreshing
    case postEditing
    case postDeleting
}
protocol PostListViewProtocol: class {
    var postListPresenter: PostListPresenterProtocol! { get set }
    
    func showLoading()
    func hideLoading()
    func showMoreEntitiesAtTheEnd(Amount:Int)
    func showEntities()
    func showMoreEntitiesAtTheStart(Amount:Int)
    func shouldReloadPostViews(atIndexes:[Int])
    func shouldDeletePostViews(atIndexes:[Int])
    func schouldShowError(status:ApiRequestStatus,activity:PostActivities)

    // PRESENTER -> VIEW
}


protocol PostListPresenterProtocol: class {
    weak var view: PostListViewProtocol? { get set }
    var interactor: PostListInteractorInputProtocol? { get set }
    var router: PostListRouterProtocol?{ get set}
    
    // VIEW -> PRESENTER
    
    func postBy(index:Int)->WallPost?
    var postsCount:Int {get}
    func viewDidLoad()
    func viewWillAppear()
    func viewDidScrollToLastLoadedPosts()
    func viewWantsToResfreshPosts()
    func viewWillDisplayObject(number:Int)
    func showEditPostInterface(post: WallPost)
    func deletePost(post: WallPost)
    func showDetailForPost(post:WallPost)
    func showSenderProfileForPost(post:WallPost)
    func showAddPostInterface()
}

protocol PostListRouterProtocol: class {
    // PRESENTER -> WIREFRAME
    var presenter:PostListPresenterProtocol? { get set }
    
    func goToSenderProfile(from view: PostListViewProtocol, forPost post: WallPost)
    func goToEditPost(from view: PostListViewProtocol,forPost post:WallPost)
    func goToPostDetail(from view: PostListViewProtocol, forPost post: WallPost)
    func goToAddPost(from view: PostListViewProtocol,wallId:Int)

}
protocol PostListInputProtocol:class
{
    func postWasRemoved(post:WallPost)
    func postWasEdited(oldPost:WallPost,newPost:WallPost)
    func newPostWasAdded(newPost:WallPost)
}

protocol PostListInteractorOutputProtocol: class {
    // INTERACTOR -> PRESENTER
    func loadEntities(data:[[String:Any]])
    func loadEntitiesToTheEnd(data:[[String:Any]])
    func loadEntitiesToTheStart(data:[[String:Any]])
    func deletedPost(post:WallPost)
    func editedPost(oldPost:WallPost,newPostDictionary:[String:Any])
    func someError(status:ApiRequestStatus,activity:PostActivities)
}

protocol PostListInteractorInputProtocol: class {
    var presenter: PostListInteractorOutputProtocol? { get set }
    var apiManager:WallPostsDataManagerProtocol? { get set}
    var wallOwnerId:Int!{get set}
    // PRESENTER -> INTERACTOR
    func load()
    func loadMoreAfter(post:WallPost)
    func loadMoreBefore(post:WallPost)
    func edit(post:WallPost)
    func delete(post:WallPost)
}

