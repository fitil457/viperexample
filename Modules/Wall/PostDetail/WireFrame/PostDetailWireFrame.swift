//
// Created by VIPER
// Copyright (c) 2017 VIPER. All rights reserved.
//

import Foundation
import UIKit
class PostDetailWireFrame: PostDetailWireFrameProtocol
{
    weak var presenter:PostDetailPresenterProtocol?
    
    func goToSenderProfile(from view: PostDetailViewProtocol, forPost post: WallPost){
        
    }
    
    func goToEditPost(from view: PostDetailViewProtocol,forPost post:WallPost)
    {
        if let vc = PostAddAssembly.generateEditModule(post: post, presenter as? PostAddModuleOutput)
        {
            if let vc0  = view as? UIViewController
            {
                vc0.show(vc, sender: nil)
            }
        }
    }
    

}
