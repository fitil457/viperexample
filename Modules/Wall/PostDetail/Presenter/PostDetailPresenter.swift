//
// Created by VIPER
// Copyright (c) 2017 VIPER. All rights reserved.
//

import Foundation

class PostDetailPresenter: PostDetailPresenterProtocol, PostDetailInteractorOutputProtocol
{
    var post:WallPost
    weak var view: PostDetailViewProtocol?
    var interactor: PostDetailInteractorInputProtocol?
    var wireFrame: PostDetailWireFrameProtocol?
    var moduleOutput:PostDetailOutput?
    func viewDidLoad()
    {
        view?.showPost(post: post)
    }
    init(post:WallPost)
    {
        self.post = post
    }
    func postWasDeleted()
    {
        moduleOutput?.deletedPost(post: post)
    }
    func showEditPostInterface(post: WallPost)
    {
        wireFrame?.goToEditPost(from: view!, forPost: post)
    }
    func deletePost(post: WallPost)
    {
        interactor?.deletePost()
    }
    func showSenderProfileForPost()
    {
        
    }
    func showAddPostInterface()
    {
        
    }
}
