//
// Created by VIPER
// Copyright (c) 2017 VIPER. All rights reserved.
//

import Foundation
import UIKit
protocol PostDetailViewProtocol: class
{
    var postDetailPresenter: PostDetailPresenterProtocol? { get set }
    
    func showPost(post:WallPost)
    func removeFromViewHierarchy()
}
protocol PostDetailWireFrameProtocol: class
{
    var presenter:PostDetailPresenterProtocol? {get set}
    
    func goToSenderProfile(from view: PostDetailViewProtocol, forPost post: WallPost)
    func goToEditPost(from view: PostDetailViewProtocol,forPost post:WallPost)
}
protocol PostDetailPresenterProtocol: class
{
    var post:WallPost{get set}
    var view: PostDetailViewProtocol? { get set }
    var interactor: PostDetailInteractorInputProtocol? { get set }
    var wireFrame: PostDetailWireFrameProtocol? { get set }
    var moduleOutput:PostDetailOutput?{get set}
    
    func viewDidLoad()
    func showEditPostInterface(post: WallPost)
    func deletePost(post: WallPost)
    func showSenderProfileForPost()
    func showAddPostInterface()
}

protocol PostDetailInteractorOutputProtocol: class
{
    func postWasDeleted()
}
protocol PostDetailInteractorInputProtocol:class
{
    var APIDataManager: WallPostsDataManagerProtocol? { get set }
    var presenter: PostDetailInteractorOutputProtocol? { get set }

    func deletePost()
}
protocol PostDetailOutput: class
{
    func deletedPost(post:WallPost)
    func editedPost(oldPost:WallPost,newPostDictionary:[String:Any])
}
