//
//  ProfilePresenter.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 04.07.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//


import UIKit

protocol ProfileViewProtocol: class {
    weak var profilePresenter: ProfilePresenterProtocol? { get set }

    // PRESENTER -> VIEW
    func showProfile(with profile: FullProfile)
    func showProfileError()
    func showProfileLoading()
    func hideProfileLoading()
}



protocol ProfilePresenterProtocol: class
{
    var view: ProfileViewProtocol? { get set }
    var interactor: ProfileInteractorInputProtocol? { get set }
    var router: ProfileRouterProtocol? { get set }
    // VIEW -> PRESENTER
    func viewDidLoad()
}

protocol ProfileInteractorOutputProtocol: class {
    // INTERACTOR -> PRESENTER
    func didLoadProfile(with profile: FullProfile)
    func showNoConnection()
}

protocol ProfileInteractorInputProtocol: class {
    weak var presenter: ProfileInteractorOutputProtocol? { get set }
    var id:Int{get set}
    var dataManager:ProfileDataManagerProtocol? { get set }
    // PRESENTER -> INTERACTOR
    func getProfile()
}

protocol ProfileRouterProtocol: class {
    // PRESENTER -> WIREFRAME
    weak var presenter:ProfilePresenterProtocol? {get set}
    func presentEditProfileScreen(from view: ProfileViewProtocol, _ output: ProfileEditOutput)
}

