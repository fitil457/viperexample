//
//  ProfileInteractor.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 07.07.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//
import UIKit
class ProfileInteractor:ProfileInteractorInputProtocol
{
    weak var presenter: ProfileInteractorOutputProtocol?
    var id:Int
    var Manager = ApiManager.shared
    var dataManager:ProfileDataManagerProtocol?

    func getProfile()
    {
        Manager.loadProfile(id: id) {[weak self] (dict, status) in
            guard let strSelf = self
                else
            {
                return
            }
            switch status
            {
            case ApiRequestStatus.success:
                guard dict != nil
                else
                {
                    return
                }
                if let prof = FullProfile(with: dict)
                {
                    strSelf.presenter?.didLoadProfile(with: prof)
                }
            case ApiRequestStatus.noConnection:
                strSelf.presenter?.showNoConnection()
                
            default:
                break
            }
        }
    }
    init(id:Int)
    {
        self.id = id
    }

}
