//
//  ProfilePresenter.swift
//  AZ-Social
//
//  Created by  Denis Ovchar on 07.07.17.
//  Copyright © 2017  Denis Ovchar. All rights reserved.
//

class ProfilePresenter:ProfilePresenterProtocol
{
    weak var view: ProfileViewProtocol?
    var interactor: ProfileInteractorInputProtocol?
    var router: ProfileRouterProtocol?
    func viewDidLoad()
    {
       
        interactor?.getProfile()
        
    }
    
}
extension ProfilePresenter:ProfileInteractorOutputProtocol
{
    func didLoadProfile(with profile: FullProfile) {
        view?.showProfile(with: profile)
    }
    func showNoConnection()
    {
        
    }
}
